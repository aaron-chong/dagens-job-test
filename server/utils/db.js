const products = require('../db');
const { v4: uuidv4 } = require('uuid');


const paginate = (items, currentPage, perPage) => {
  const page = parseInt(currentPage || 1)
  const  offset = (page - 1) * perPage
  const paginatedItems = items.slice(offset).slice(0, perPage)
  const  total_pages = Math.ceil(items.length / perPage);
  return {
    currentPage: page,
    pre_page: page - 1 ? page - 1 : null,
    next_page: total_pages > page ? page + 1 : null,
    totalProducts:paginatedItems.length,
    total_pages: total_pages,
    products: paginatedItems,
  };
};



const createProduct = (product) => {
  products.push({ id: uuidv4(), ...product });
};

const getProducts = (query) => {
  let filteredProducts = products;

  const { category, minPrice, maxPrice, page_number } = query;

  if (category) {
    filteredProducts = filteredProducts.filter((product) => {
      return category.includes(product.category);
    });
  }

  if (minPrice) {
    filteredProducts = filteredProducts.filter((product) => {
      return product.price >= minPrice;
    });
  }

  if (maxPrice) {
    filteredProducts = filteredProducts.filter((product) => {
      return product.price <= maxPrice;
    });
  }

  filteredProducts = paginate(filteredProducts, page_number, 24);

  return filteredProducts;
};


const getProductsWithSimilarPrice = (productId) => {

  const sortedProducts = products.sort((a, b) => {
    return a.price - b.price;
  })

  const productIndex = sortedProducts.findIndex((product) => product.id === productId);

  const similarPricedProducts = [...sortedProducts.slice(productIndex - 3, productIndex), ...sortedProducts.slice(productIndex + 1, productIndex + 4)];
  
  return similarPricedProducts;

}

module.exports = {
  createProduct,
  getProducts,
  getProductsWithSimilarPrice
};
