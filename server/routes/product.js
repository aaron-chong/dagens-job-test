const { Router } = require('express');
const router = Router();

const {
  createProduct,
  getProducts,
  getProductsWithSimilarPrice,
} = require('../utils/db');

router.get('/', async (req, res) => {
  try {
    const filteredProducts = getProducts(req.query);
    res.json({
      status: true,
      message: 'Products fetched successfully.',
      data: filteredProducts,
    });
  } catch (e) {
    console.log(e);
    res.json({
      status: false,
      message: 'Fail to fetch products.',
    });
  }
});

router.get('/similarpriced', async (req, res) => {
  if (!req.query.id) {
    res.json({
      status: false,
      message: 'Please provide the Product Id',
    });
  }

  try {
    const products = getProductsWithSimilarPrice(req.query.id);
    res.json({
      status: true,
      message: 'Products fetched successfully.',
      products,
    });
  } catch (e) {
    console.log(e);
    res.json({
      status: false,
      message: 'Failed to create Product',
    });
  }
});

router.post('/', async (req, res) => {
  try {
    createProduct(req.body);
    res.json({
      status: true,
      message: 'Product created successfully.',
    });
  } catch (e) {
    console.log(e);
    res.json({
      status: false,
      message: 'Failed to create Product',
    });
  }
});

module.exports = router;
