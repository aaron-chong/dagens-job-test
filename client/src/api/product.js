const API_URL = 'http://localhost:3001';

export const createProduct = (product) => {
  return fetch(`${API_URL}/api/product`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(product),
  }).then((response) => response.json());
};
