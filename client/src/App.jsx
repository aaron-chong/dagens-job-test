import React from 'react';
import Layout from './components/Layout';
import CreateProduct from './views/CreateProduct';

const App = () => {
  return <Layout><CreateProduct/></Layout>;
};

export default App;
