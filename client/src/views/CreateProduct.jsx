import React from 'react';
import { Flex, Heading } from '@chakra-ui/react'
import CreateProductForm from '../components/CreateProductForm';

const CreateProduct = () => {
  return (
    <Flex flexDir='column'>
      <Heading fontSize='lg'>Create Product</Heading>
      <CreateProductForm/>
    </Flex>
  );
};

export default CreateProduct;