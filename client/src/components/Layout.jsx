import React from 'react';
import { Container } from '@chakra-ui/react'

const Layout = ({children}) => <Container maxW='container.sm'>{children}</Container>;

export default Layout;
