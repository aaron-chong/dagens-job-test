import React from 'react';
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Select,
  Button,
  useToast,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { createProduct } from '../api/product';

const CreateProductForm = () => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
    reset,
  } = useForm();

  const toast = useToast();

  const onSubmit = (values) => {
    return new Promise((resolve, reject) => {
      createProduct(values).then((res) => {
        if (res.status) {
          toast({
            title: res.message,
            status: 'success',
            duration: 9000,
            isClosable: true,
            position: 'top',
          });
          resolve();
          reset();
        } else {
          toast({
            title: res.message,
            status: 'error',
            duration: 9000,
            isClosable: true,
            position: 'top',
          });
          reject();
        }
      });
    });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl isInvalid={errors.name} mt='30px'>
        <FormLabel htmlFor='name'>Name</FormLabel>
        <Input
          id='name'
          placeholder='Name'
          {...register('name', {
            required: 'Name is required',
          })}
        />
        <FormErrorMessage>
          {errors.name && errors.name.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.name} mt='30px'>
        <FormLabel htmlFor='name'>Category</FormLabel>
        <Select
          placeholder='Select category'
          {...register('category', {
            required: 'Category is required',
          })}
        >
          <option value='meat'>Meat</option>
          <option value='green'>Green</option>
          <option value='fish'>Fish</option>
        </Select>{' '}
        <FormErrorMessage>
          {errors.category && errors.category.message}
        </FormErrorMessage>
      </FormControl>
      <Button mt={4} colorScheme='teal' isLoading={isSubmitting} type='submit'>
        Submit
      </Button>
    </form>
  );
};

export default CreateProductForm;
